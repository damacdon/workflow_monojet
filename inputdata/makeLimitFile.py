import ROOT
import sys

infile = sys.argv[1]

f=ROOT.TFile(infile)
tree = f.Get("stats")

obsL = tree.GetLeaf("obs_upperlimit");
medL = tree.GetLeaf("exp_upperlimit");
m2L = tree.GetLeaf("exp_upperlimit_minus2");
m1L = tree.GetLeaf("exp_upperlimit_minus1");
p1L = tree.GetLeaf("exp_upperlimit_plus1");
p2L = tree.GetLeaf("exp_upperlimit_plus2");


tree.GetEntry(0);

med = medL.GetValue()
obs = obsL.GetValue()
p2 = p2L.GetValue()
p1 = p1L.GetValue()
m1 = m1L.GetValue()
m2 = m2L.GetValue()

outfile = open('/jdm/Interpretation/CommonStatTools/LimitDir/asymptotics/limit_CL95.txt', 'w')
outfile.write("Expected limit: %s + %s - %s\n"%(med, p1-med, med-m1))
outfile.write("Observed limit: %s\n\n"%obs)
outfile.write("obs -2s -1s exp +1s +2s\n")
outfile.write("%.2f %.2f %.2f %.2f %.2f %.2f"%(obs, m2, m1, med, p1, p2))
