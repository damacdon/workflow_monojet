# Workflow

## Current Main Developers

Do not hesitate to contact one of the following individuals with questions:

  * Danika MacDonell (danikam1@uvic.ca)

## Summary

This repository currently contains the steps and workflow needed to pass an arbitrary signal DAOD through the monojet analysis chain from the DAOD file to a python dictionary, which is input to the Interpretation stage for analysis and limit setting. Currently, the workflow is set up to run entirely on the local machine.


## Prerequisites

To run the workflow on your machine, the following prerequisites are needed:

1. A running [docker](https://docs.docker.com/install/) installation.
2. A [yadage](https://yadage.readthedocs.io/en/latest/introduction.html) installation. Alternatively, yadage commands (eg. packtivity-run, yadage-run, etc.) can be run from inside the official `yadage` docker container by executing the following in the terminal in which you plan to run them:

```bash
docker run --rm -it -e PACKTIVITY_WITHIN_DOCKER=true -v $PWD:$PWD -w $PWD -v /var/run/docker.sock:/var/run/docker.sock yadage/yadage sh
docker login gitlab-registry.cern.ch -u [your CERN username]
```

You will be asked for your CERN password.

## Instructions for Use

#### 1. Clone this repository to your machine and pull the docker images:

Clone the workflow repository as follows:

```bash
git clone https://gitlab.cern.ch/atlas-phys-exotics-monojet/workflow.git
```

You will be asked for your CERN credentials to clone the repo.

First, log in to docker to pull the docker images used in the workflow steps to your computer:

```bash
docker login gitlab-registry.cern.ch -u [your CERN username]
```

You will be asked for your CERN password. The images are then pulled as follows:

```bash
docker pull [docker image name]
```

Where the docker image names to pull are:
  * gitlab-registry.cern.ch/atlas-phys/exot/jdm/ana-exot-2018-06/monojet:master
  * gitlab-registry.cern.ch/atlas-phys/exot/jdm/ana-exot-2018-06/monojetreader:master
  * gitlab-registry.cern.ch/damacdon/monojetreader:master
  * gitlab-registry.cern.ch/atlas-phys/exot/jdm/ana-exot-2018-06/vjetsreweightingtool:master
  * gitlab-registry.cern.ch/atlas-phys/exot/jdm/ana-exot-2018-06/interpretation:master

#### 2. Obtain the signal DAODs for each epoch from Rucio (on, eg. lxplus) if needed.

These instructions assume that EXOT5 DAODs have been produced for the signal in question for each run2 epoch (mc16a, mc16d, and mc16e), and that the DAODs are available on rucio.

Suppose, for example, that the DAODs are `mc16_13TeV.311454.PowhegPy8EG_A14N23LO_DMA_1_10_gq0p25.deriv.DAOD_EXOT5.e7368_a875_r[...]_p3712`, where the `[...]` will be `9364` (mc16a), `10201` (mc16d), or `10724` (mc16d). The DOADs can be obtained from lxplus using Rucio as follows:

```bash
ssh [your_cern_username]]@lxplus.cern.ch
setupATLAS
lsetup rucio
voms-proxy-init -voms atlas
```

If you don't have a cern grid certificate, you can follow the instructions [here](https://www.racf.bnl.gov/docs/howto/grid/installcert) to obtain a certificate, export it to your browser, register it with VOMS, and convert the certificate for use with rucio and grid job submission.

Next, create a directory named MJ_RECAST under /tmp, and download the DOAD files to the directory:

```bash
mkdir /tmp/MJ_RECAST
rucio download --dir /tmp/MJ_RECAST mc16_13TeV.311454.PowhegPy8EG_A14N23LO_DMA_1_10_gq0p25.deriv.DAOD_EXOT5.e7368_a875_r*_p3712
```

If you want to process several DAODs, make a file (eg. RECAST_DAODs.txt), and add the names of the DAODs to the file, with one DAOD name per line. The rucio command is then:

```bash
rucio download --dir /tmp/MJ_RECAST `cat RECAST_DAODs.txt`
```

#### 3. Download the DAOD files and HistFitter repo.

Download the DAOD files from lxplus (or wherever rucio was running) into the `inputdata/DAODs/mc16[...]` directory. Eg. if the rucio command was run from node 081 of lxplus (lxplus081), then from the directory containing this README:

```bash
scp -r [your_cern_username]@lxplus081.cern.ch:/tmp/MJ_RECAST/*r9364* inputdata/DAODs/mc16a
scp -r [your_cern_username]@lxplus081.cern.ch:/tmp/MJ_RECAST/*r10201* inputdata/DAODs/mc16d
scp -r [your_cern_username]@lxplus081.cern.ch:/tmp/MJ_RECAST/*r10724* inputdata/DAODs/mc16e
```

If you want to specify a anti-SF file and/or cross-section file when creating shorttrees, copy the anti-SF file and/or cross-section file to `inputdata/antiSF/antisf_recast.txt` and `inputdata/xSection/recast_xSections.txt`, respectively. The format of the cross section file should follow that of eg. `MonoJetReader/db_files/susy_crosssections.txt`, and the format of the anti-SF file shoudl follow that of eg. `MonoJetReader/db_files/mc16a_MJ12Nov18.txt`.

If an anti-SF file is not specified, the corresponding weights will be set to 1. If a cross-section file is not specified, the cross-section file will default to `MonoJetReader/db_files/susy_crosssections.txt`.


#### 4. Run the workflow using yadage-run.

Once all the needed files are copied, the full workflow can be run as follows:

```bash
rm -rf workdir
yadage-run workdir workflow.yml -p run_num='311454' -p DAODs_mc16a='DAODs/mc16a' -p DAODs_mc16d='DAODs/mc16d' -p DAODs_mc16e='DAODs/mc16e' -p xSec_dir='xSection' -p antiSF_dir='antiSF' -p makeLimitScript='makeLimitFile.py' -d initdir=$PWD/inputdata --visualize
```

where `run_num` is the run number associated with the signal DAOD (311454 for the example considered in Section 2)

The shell scripts that were executed before the yadage-run command are needed to clean the workspace of log files and output files from any previous runs.

The full workflow with the sample DAOD listed in Section 2 took ~20h to run with all the systematics on a machine with 30G of RAM and good internet connection (the first step of DAOD skimming took the vast majority of that time). For a quick test run, you can try limiting the number of events to process to something small, like 1000 - this should reduce the time to less than 1h. To do so, add the following option to line 26 in steps.yml ('python source/MonoJetAnalysis/...'): '-m 1000'. You can also skip running over the systematics if desired by removing the options --doSyst and --doSystTrees from line 26 in steps.yml.

##### Summary of Interpretation output

Once the workflow is finished, the output from each stage in the workflow.yml file should be contained within the corresponding sub-directory in the workdir directory. For example, the minitrees produced by the skimming_stage step will be stored in `workdir/skimming_stage/SampleSignal`.

The output of the final interpretation step is contained in four directories under `workdir/interpretation_stage/Interpretation_output`:

* `results` contains the HistFitter workspace
* `SysPlots` contains plots of all background systematics as a function of MET
* `PullPlots` contains the post-fit pull plots for the systematics
* `LimitDir` contains the results of running the limit-setting script `Scripts/quickLimits.sh`
   * `asymptotics/test_*.root` are the output of running `runAsymptoticsCLs.C` in CommonStatTools, and `asymptotics/limit_CL95.txt` summarizes the contents of `asymptotics/test_CL95.root`
   * the `corrmatrix` directory contains correlation matrices between the systematics produced by `getCorrMatrix.C`
   * the `crosschecks` directory contains cross checks produced by `runFitCrossCheckForLimits.C`

##### Yadage Workflow Visualization

The `--visualize` option in the above `yadage-run` command produces a visualization of the workflow. These will be called `yadage_workflow_instance.pdf` and `yadage_workflow_instance.png`, and will contained in the `workdir/_yadage` directory.

#### 5. Use packtivity-run to run steps individually, if needed.

The packtivity-run command is used to run individual steps in the workflow, as listed in steps.yml. The packtivity-run command for each step is listed in the comments just above the step. For example, the first step, "skimming_stage", can be run on its own using the following commands:

```bash
./clear_output.sh skimming_stage
packtivity-run steps.yml#/skimming_stage -p input_directory="'$PWD/inputdata/DAODs/mc16a'" -p run_number="311454" -p output_directory="'{workdir}/SampleSignal_mc16a'" -p monojet_conf="'{workdir}/monojet_mc16a.conf'" --write workdir/skimming_stage --read workdir/skimming_stage_mc16a```
```

Note that most steps will fail if the prior steps listed in steps.yml haven't yet been run. 

The clear_output.sh script with the 'skimming_stage' argument removes any output files that may have been produced in the workdir/skimming_stage directory in previous runs before executing this step. You can type

```bash
./clear_output.sh -h
```

to see the list of all available arguments. 