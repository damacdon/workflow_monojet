#!/bin/bash
####################################################################
#
# Script to clear the output for either a given stage, or all stages
#
####################################################################

# Function to check if an item $2 is contained in a list $1
# From https://jaytaylor.com/notes/node/1360360709000.html
function contains() {
  local n=$#
  local value=${!n}
  for ((i=1;i < $#;i++)) {
    if [ "${!i}" == "${value}" ]; then
      echo "y"
      return 0
    fi
  }
  echo "n"
  return 1
}

# List of allowed arguments
allowed_args=("all_stages" "skimming_stage" "shorttree_stage" "uncertainty_tool_stage" "histogram_stage" "make_dict_stage" "interpretation_stage")

# Check for help request or bad input, in which case print help message
if [ "$1" = "-h" ] || [ "$1" = "--help" ] || [ "$#" -eq 0 ] || [ `contains ${allowed_args[@]} $1` = "n" ]
  then
    printf "\n###########################################################\n"
    printf "Usage of clear_output.sh:\n"
    printf "\nUSAGE 1: To clear the output from a given stage:\n\t>> ./clear_output.sh [stage]\n\n"
    printf "\tAvailable stages: "
    printf '\n\t\t- %s ' "${allowed_args[@]:1}"
    printf "\n\n\tExample: \n\t>> ./clear_output.sh skimming_stage\n"
    printf "\nUSAGE 2: To clear the output from all stages:\n\t>> ./clear_output.sh all_stages\n"
    printf "###########################################################\n\n"
    exit 1
fi

# Go through the stages that can be cleared
if [ "$1" = "skimming_stage" ] ; then
  printf "Are you sure you want to clear the output from the skimming stages? (y/n)\n"
  read reply

  if [ $reply = "y" ]
    then
      rm -rf workdir/skimming_stage*/*
    else
      exit 1
  fi
fi

if [ "$1" = "shorttree_stage" ] ; then
  printf "Are you sure you want to clear the output from the shorttree stages? (y/n)\n"
  read reply

  if [ $reply = "y" ]
    then
      rm -rf workdir/shorttree_stage*/*
    else
      exit 1
  fi
fi

if [ "$1" = "uncertainty_tool_stage" ] ; then
  printf "Are you sure you want to clear the output from the uncertainty tool stage? (y/n)\n"
  read reply

  if [ $reply = "y" ]
    then
      rm -rf workdir/uncertainty_tool_stage/*
    else
      exit 1
  fi
fi

if [ "$1" = "histogram_stage" ] ; then
  printf "Are you sure you want to clear the output from the histogram stages? (y/n)\n"
  read reply

  if [ $reply = "y" ]
    then
      rm -f workdir/histogram_stage*/*
    else
      exit 1
  fi
fi

if [ "$1" = "make_dict_stage" ] ; then
  printf "Are you sure you want to clear the output from the make dict stage? (y/n)\n"
  read reply

  if [ $reply = "y" ]
    then
      rm -f workdir/make_dict_stage*/*
    else
      exit 1
  fi
fi

if [ "$1" = "interpretation_stage" ] ; then
  printf "Are you sure you want to clear the output from the interpretation stage? (y/n)\n"
  read reply

  if [ $reply = "y" ]
    then
      rm -rf workdir/interpretation_stage/*
    else
      exit 1
  fi
fi

if [ "$1" = "all_stages" ] ; then
  printf "Are you sure you want to clear the output from all stages? (y/n)\n"
  read reply

  if [ $reply = "y" ]
    then
      rm -rf workdir/skimming_stage/* workdir/shorttree_stage/* workdir/uncertainty_tool_stage/* workdir/histogram_stage/* workdir/make_dict_stage/* workdir/interpretation_stage/*
    else
      exit 1
  fi
fi
